/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day11carowners;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Database {

    final private Connection dbConn;

    Database() throws SQLException {
        dbConn = DriverManager.getConnection("jdbc:mysql://localhost/ipd20carowners?user=root&password=root");
    }

    ArrayList<Owner> getOwners() throws SQLException {
        // return owners and number of cars they own
        // SELECT *,COUNT(*) as carsOwned JOIN Owners onto Cars, GROUP BY ownerId
        try {
            ArrayList<Owner> td = new ArrayList<>();
            String sql = "select \n"
                    + "id,name,photo,\n"
                    + "(select count(*) from cars where ownerId=o.id) as carsOwned\n"
                    + "from owners o ";

            try ( PreparedStatement select = dbConn.prepareStatement(sql);  ResultSet resultSet = select.executeQuery()) {
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String name = resultSet.getString("name");
                    int carsOwned = resultSet.getInt("carsOwned");
                    byte[] photo = resultSet.getBytes("photo");

                    Owner t = new Owner(id, name, photo, carsOwned);

                    td.add(t);
                }
            }
            return td;
        } catch (InvalidValueException ex) {
            throw new SQLException(ex.getMessage());
        }
    }

    ArrayList<Car> getCars(int ownerId) throws SQLException {
        // return cars and possibly their owner names
        // SELECT * LEFT OUTER JOIN Cars onto Owner

        ArrayList<Car> td = new ArrayList<>();
        String sql = "select \n"
                + "c.id,makeModel,prodYear,plates,o.name,c.ownerId \n"
                + "from cars c left join owners o on c.ownerId=o.id \n";

        if (ownerId != 0) {
            sql += " where c.ownerId=" + ownerId;
        }

        PreparedStatement select = dbConn.prepareStatement(sql);
        ResultSet resultSet = select.executeQuery();
        while (resultSet.next()) {
            //int id, int ownerId, String makeModel, int prodYear, String plates, String ownerName
            int id = resultSet.getInt("id");
            int ownerID = resultSet.getInt("ownerId");
            String makeModel = resultSet.getString("makeModel");
            int prodYear = resultSet.getInt("prodYear");
            String plates = resultSet.getString("plates");
            String name = resultSet.getString("name");
            if (plates == null) {
                plates = "";
            }
            try {
                Car t = new Car(id, ownerID, makeModel, prodYear, plates, name);

                td.add(t);
            } catch (InvalidValueException ex) {
                throw new SQLException(ex.getMessage());
            }
        }
        return td;
    }

    Owner getOwner(int OwnerId) throws SQLException, InvalidValueException {
        Owner owner = null;
        String sql = "select \n"
                + "id,name,photo,\n"
                + "(select count(*) from cars where ownerId=o.id) as carsOwned\n"
                + "from owners o where id=" + OwnerId;
        PreparedStatement select = dbConn.prepareStatement(sql);
        ResultSet resultSet = select.executeQuery();
        if (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            int carsOwned = resultSet.getInt("carsOwned");
            byte[] photo = resultSet.getBytes("photo");

            owner = new Owner(id, name, photo, carsOwned);

        }
        return owner;
    }

    void updateOwner(Owner owner) throws SQLException {
        // select first to check that owner name is not used yet
        // throw SQLException if check fails
        String name = owner.getName();
        int id = owner.getId();
        String sql = String.format("select * from owners where name ='%s' and id<>%d", name, id);
        PreparedStatement select = dbConn.prepareStatement(sql);
        ResultSet resultSet = select.executeQuery();
        if (resultSet.next()) {
            throw new SQLException("The name is existing!");
        } else {
            PreparedStatement update = dbConn.prepareStatement("update owners set name=?,photo=? where id=?");
            update.setString(1, owner.getName());
            update.setBytes(2, owner.getPhoto());
            update.setInt(3, owner.getId());
            update.execute();
        }
    }

    void addOwner(Owner owner) throws SQLException {
        // select first to check that owner name is not used yet
        // or that the only one with that name is owner.id
        // throw SQLException if check fails
        String name = owner.getName();
        String sql = String.format("select * from owners where name ='%s'", name);
        PreparedStatement select = dbConn.prepareStatement(sql);
        ResultSet resultSet = select.executeQuery();
        if (resultSet.next()) {
            throw new SQLException("The name is existing!");
        } else {
            PreparedStatement insert = dbConn.prepareStatement("insert into owners values(NULL,?,?)");
            insert.setString(1, owner.getName());
            insert.setBytes(2, owner.getPhoto());
            insert.execute();
        }
    }

    void delOwner(Owner owner) throws SQLException {
        PreparedStatement delete = dbConn.prepareStatement("delete from owners where id=?");
        delete.setInt(1, owner.getId());
        delete.execute();

    }

    void giveUpCar(Car car) throws SQLException {
        PreparedStatement update = dbConn.prepareStatement("update cars set ownerId=null where id=?");
        update.setInt(1, car.getId());
        update.execute();
    }

    void updateCar(Car car) throws SQLException {
        // select first to check that plates are not used yet
        // throw SQLException if check fails
        String plate = car.getPlates();
        if (!plate.equals("")) {
            String sql = String.format("select * from cars where plates ='%s' and id<>%d", plate, car.getId());
            PreparedStatement select = dbConn.prepareStatement(sql);
            ResultSet resultSet = select.executeQuery();
            if (resultSet.next()) {
                throw new SQLException("The plate is existing!");
            }
            PreparedStatement update = dbConn.prepareStatement("update cars set makeModel=?,prodYear=?,plates=? where id=?");
            update.setString(1, car.getMakeModel());
            update.setInt(2, car.getProdYear());
            update.setString(3, plate);
            update.setInt(4, car.getId());
            update.execute();

        } else {
            PreparedStatement update = dbConn.prepareStatement("update cars set makeModel=?,prodYear=?,plates=NULL where id=?");
            update.setString(1, car.getMakeModel());
            update.setInt(2, car.getProdYear());
            update.setInt(3, car.getId());
            update.execute();
        }

    }

    void addCar(Car car) throws SQLException {
        // select first to check that plates are not used yet
        // or that the only one with those plates is car.id
        // throw SQLException if check fails
        String plate = car.getPlates();
        if (!plate.equals("")) {
            String sql = String.format("select * from cars where plates ='%s'", plate);
            PreparedStatement select = dbConn.prepareStatement(sql);
            ResultSet resultSet = select.executeQuery();
            if (resultSet.next()) {
                throw new SQLException("The plate is existing!");
            } else {
                PreparedStatement insert = dbConn.prepareStatement("insert into cars values(NULL,NULL,?,?,?)");
                insert.setString(1, car.getMakeModel());
                insert.setInt(2, car.getProdYear());
                insert.setString(3, car.getPlates());
                insert.execute();
            }

        } else {
            PreparedStatement insert = dbConn.prepareStatement("insert into cars values(NULL,NULL,?,?,NULL)");
            insert.setString(1, car.getMakeModel());
            insert.setInt(2, car.getProdYear());
            insert.execute();
        }
    }

    void deleteCar(int id) throws SQLException {
        PreparedStatement delete = dbConn.prepareStatement("delete from cars where id=?");
        delete.setInt(1, id);
        delete.execute();
    }

    void UpdateCarOwner(int Carid, int Ownerid) throws SQLException {
        // consider database settings, one of:
        // RESTRICT | CASCADE | SET NULL (preferred in this case) | NO ACTION | SET DEFAULT
        if (Ownerid == 0) {
            PreparedStatement delete = dbConn.prepareStatement("update cars set ownerId=null where id=?");
            delete.setInt(1, Carid);
            delete.execute();
        } else {
            PreparedStatement update = dbConn.prepareStatement("update cars set ownerId=? where id=?");
            update.setInt(1, Ownerid);
            update.setInt(2, Carid);
            update.execute();
        }
    }
}
