package quiz1todo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Todo {
    private int id;
    private	String task; // 1-100 characters, made up of uppercase and lowercase letters, digits, space, _-*?%#@(),./\ only
    private	int difficulty; // 1-5, as slider
    private	Date dueDate; // year 1900-2100 both inclusive, use formatted field
    private	Status status; // enum, combo box
        
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    static enum Status { Pending, Done, Delegated };
    final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        
    public Todo(String dataLine) throws InvalidValueException {
        try {
            String[] data = dataLine.split(";");
            if (data.length != 4) {
                throw new InvalidValueException("Invalid number of items in data line");
            }
            setTask(data[0]); // ex
            setDifficulty(Integer.parseInt(data[1])); // ex NumberFormatException
            setDueDate(dateFormat.parse(data[2])); // ex ParseException
            setStatus(Status.valueOf(data[3]));
        } catch (ParseException|IllegalArgumentException  ex) {
            throw new InvalidValueException("Parsing error");
        }
    }
        
    public Todo(String task, int difficulty, Date dueDate, Status status) throws InvalidValueException {
        setTask(task);
        setDifficulty(difficulty);
        setDueDate(dueDate);
        setStatus(status);
    }

    public void setTask(String task) throws InvalidValueException{
        if(!task.matches("^[A-Za-z\\d\\s_\\*\\?\\%\\#\\@\\(\\)\\,\\.\\/\\\\-]{1,100}$")){//- must be last char
            throw new InvalidValueException("Task must be between 1-100 only some characters.");
        }
        this.task = task;
    }

    public void setDifficulty(int difficulty) throws InvalidValueException {
        if (difficulty < 0 || difficulty > 5) {
            throw new InvalidValueException("Difficulty must be 0-5");
        }
        this.difficulty = difficulty;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getTask() {
        return task;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Status getStatus() {
        return status;
    }
        
    String toDataString() {
        return String.format("%d:%s;%d;%s;%s", id, task, difficulty, dateFormat.format(dueDate),status);
    }
    
    @Override
    public String toString() {
        return String.format("%d: %s by %s / diff.%d %s",id,task, dateFormat.format(dueDate),difficulty,status);
    }
}

class InvalidValueException extends Exception {
	InvalidValueException(String msg) { super(msg); }
}
