/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgfinal;


/**
 *
 * @author ipd
 */
public class Card {
    private int id;
    private String permCode;
    private byte[] photo;

    public void setId(int id) {
        this.id = id;
    }

    public void setPermCode(String permCode) throws InvalidValueException {
        if(!permCode.matches("^[A-Z]{4}[0-9]{10}$"))
            throw new InvalidValueException("The perm code must be AAAA1234567890 format");
        this.permCode = permCode;
    }

    public void setPhoto(byte[] photo) throws InvalidValueException {
        if(photo.length==0)
            throw new InvalidValueException("The photo must be updloaded");
        this.photo = photo;
    }

    public Card(int id, String permCode, byte[] photo) throws InvalidValueException {
        setId(id);
        setPermCode(permCode);
        setPhoto(photo);
    }

    public int getId() {
        return id;
    }

    public String getPermCode() {
        return permCode;
    }

    public byte[] getPhoto() {
        return photo;
    }    
    
}
