/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgfinal;


import com.mysql.cj.xdevapi.Statement;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author ipd
 */
public class Database {

    final private Connection dbConn;

    Database() throws SQLException {
        dbConn = DriverManager.getConnection("jdbc:mysql://localhost/final?user=root&password=root");
    }

    ArrayList<Student> getStudents() throws SQLException {

        ArrayList<Student> td = new ArrayList<>();
        PreparedStatement select = dbConn.prepareStatement("SELECT s.id,name,gradesList,s.cardId,permCode,photo FROM students s LEFT JOIN cards c ON s.cardId = c.id");
        ResultSet resultSet = select.executeQuery();
        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String gradesList = resultSet.getString("gradesList");
            int cardId = resultSet.getInt("cardId");
            String permCode = resultSet.getString("permCode");
            byte[] photo = resultSet.getBytes("photo");
            //int id, String name, String gradesList, Card card
            //int id, String permCodeString, byte[] photo
            try {
                Card card = null;
                if (cardId != 0) {
                    card = new Card(id, permCode, photo);
                }
                Student student = new Student(id, name, gradesList, card);
                td.add(student);
            } catch (InvalidValueException ex) {
                throw new SQLException("Cannot create student.");
            }
        }
        resultSet.close();
        select.close();
        return td;
    }

    void addStudent(Student student) throws SQLException {
        PreparedStatement insert = dbConn.prepareStatement("INSERT INTO students values(NULL,?,?,null)");

        insert.setString(1, student.getName());
        insert.setString(2, student.getGradesList());
        insert.execute();
        insert.close();
    }

    void updateStudent(Student student) throws SQLException {
        PreparedStatement update = dbConn.prepareStatement("UPDATE students SET name=?,gradesList=? WHERE id=?");
        update.setString(1, student.getName());
        update.setString(2, student.getGradesList());
        update.setInt(3, student.getId());
        update.execute();
        update.close();
    }

    void addCard(Student student) throws SQLException {
        PreparedStatement selSta = dbConn.prepareStatement("SELECT permCode FROM cards WHERE permCode=?");
        selSta.setString(1, student.getCard().getPermCode());
        ResultSet rs = selSta.executeQuery();
        if (rs.next()) {
            throw new SQLException("The perm Code is existing");
        }
        rs.close();
        selSta.close();

        //PreparedStatement insert = dbConn.prepareStatement("INSERT INTO cards values(NULL,?,?)",Statement.RETURN_GENERATED_KEYS);
        PreparedStatement insert = dbConn.prepareStatement("INSERT INTO cards values(NULL,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
        insert.setString(1, student.getCard().getPermCode());
        insert.setBytes(2, student.getCard().getPhoto());
        insert.execute();
        ResultSet rst = insert.getGeneratedKeys();
        int cardId=0;
        if (rst.next()) {
            int lastInsertedId = rst.getInt(1);
            cardId = lastInsertedId;
            PreparedStatement update = dbConn.prepareStatement("UPDATE students SET cardId=? WHERE id=?");
            update.setInt(1, cardId);
            update.setInt(2, student.getId());
            update.execute();
            update.close();
        } else {
            throw new SQLException("Error getting last insert id");
        }
        rst.close();
        insert.close();
    }
}
