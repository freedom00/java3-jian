/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkgfinal;

import java.util.ArrayList;

/**
 *
 * @author ipd
 */

public class Student {
    private int id;
    private String name;
    private String gradesList;
    private Card card;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) throws InvalidValueException {
        if(name.length()<2 || name.length()>100)
            throw new InvalidValueException("The name must be 2-100 chars");
        this.name = name;
    }

    public void setGradesList(String gradesList) throws InvalidValueException {
        if(gradesList.length()<=0 || gradesList.length()>1000)
            throw new InvalidValueException("The grades mist be 1-1000 chars");
        if(!gradesList.matches("^[A-F\\+\\s\\,\\t\\n-]{1,1000}$"))
            throw new InvalidValueException("The grades format is wrong");
        this.gradesList = gradesList;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Student(int id, String name, String gradesList, Card card) throws InvalidValueException {
        setId(id);
        setName(name);
        setGradesList(gradesList);
        setCard(card);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getGradesList() {
        return gradesList;
    }

    public Card getCard() {
        return card;
    }

    @Override
    public String toString() {
        try{
            return String.format("%d: %s(%s) GPA=%.2f", id,name,card.getPermCode(),calculateGPA(gradesList));
        }catch(NullPointerException ex){
            return String.format("%d: %s(no card) GPA=%.2f", id,name,calculateGPA(gradesList));
        }        
    }
    
    static double ConvertGPA(String str){
        switch (str){
            case "A+":
                return 4.0;
            case "A":
                return 4.0;
            case "A-":
                return 3.7;
            case "B+":
                return 3.3;
            case "B":
                return 3.0;
            case "B-":
                return 2.7;
            case "C+":
                return 2.3;
            case "C":
                return 2.0;
            case "C-":
                return 1.7;
            case "D+":
                return 1.3;
            case "D":
                return 1.0;
            case "E":
                return 0.0;
            case "F":
                return 0.0;
            default:
                return 0.0;
        }
    }
    
    static double calculateGPA(String grades){
        ArrayList<String> arr = new ArrayList<>();
        String tem=grades;
        int j=0;
        while (tem.contains("\n") ||tem.contains("\t") ||tem.contains(",") || tem.contains(" ")){
            char c = tem.charAt(j);
            if(c==' '||c==','||c=='\n'|c=='\t'){
                String sub = tem.substring(0,j);
                tem = tem.substring(j+1,tem.length());
                arr.add(sub);
                j=0;
            }else
                j++;
        }
        if(!tem.equals(""))
            arr.add(tem);
        double grade=0.0;
        for(int i=0;i<arr.size();i++){
            String strg = arr.get(i);
            grade += ConvertGPA(strg);
        }
        grade = grade/arr.size();
        return grade;
    }
    
}

class InvalidValueException extends Exception {
	InvalidValueException(String msg) { super(msg); }
}