package dya05peoplesorted;

import com.sun.tools.attach.AgentLoadException;
import java.util.Comparator;
import java.util.jar.Attributes;

class ValueInvalidException extends Exception {
    public ValueInvalidException(String msg) {
        super(msg);
    }
}

public class Person implements Comparable<Person>{
    String name;
    int age;
    double weightKg;

    public Person(String name,int age,double weightKg) throws ValueInvalidException {
        setName(name);
        setAge(age);
        setWeightKg(weightKg);
    }

    public void setName(String name) throws ValueInvalidException {
        if (!isNameValid(name)) {
            throw new ValueInvalidException("Name invalid");
        }
        this.name = name;
    }

    public static boolean isNameValid(String nameToTest) {
        if (nameToTest.length() < 1 || nameToTest.length() > 50) {
            return false;
        }
        // TODO: add other requirements
        return true;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWeightKg(double weightKg) {
        this.weightKg = weightKg;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getWeightKg() {
        return weightKg;
    }
    
    
    
    @Override
    public int compareTo(Person other) { // by name
        return name.compareTo(other.name);
    }

    // anonymous class used here as comparator instance
    final static Comparator<Person> comparatorByName = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            return p1.name.compareTo(p2.name);
        }
    };

    final static Comparator<Person> comparatorByAge = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            return p1.age - p2.age;
        }
    };
    
    final static Comparator<Person> comparatorByWeight = new Comparator<Person>() {
        @Override
        public int compare(Person p1, Person p2) {
            if (p1.weightKg == p2.weightKg) {
                return 0;
            }
            if (p1.weightKg > p2.weightKg) {
                return 1;
            } else {
                return -1;
            }
        }

    };

    @Override
    public String toString(){
        return String.format("%s %d %.2f",name,age, weightKg);
    }
}
