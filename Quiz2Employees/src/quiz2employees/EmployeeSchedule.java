/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package quiz2employees;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


class InvalidValueException extends Exception {
  InvalidValueException(String msg) { super(msg); }
}

enum Weekday { Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday }

public class EmployeeSchedule {
    String name; // 2-50 characters, not permitted are: ;^?@!~*
    boolean isManager;
    String department; // 2-50 characters, not permitted are: ;^?@!~*
    Date dateHired; // year between 1900 and 2100
    ArrayList<Weekday> workdaysList = new ArrayList<>(); // no duplicates allowed
    
    EmployeeSchedule(String name, boolean isManager, String department, Date dateHired) throws InvalidValueException {
            setName(name);
            setIsManager(isManager);
            setDateHired(dateHired);
            setDepartment(department);
    }

    EmployeeSchedule(String dataLine) throws InvalidValueException{
        try {
            String[] data = dataLine.split(";");
            if (data.length != 4) {
                throw new InvalidValueException("Invalid number of items in data line");
            }
            setName((data[0].indexOf("*")!=-1)?data[0].substring(0,data[0].length()-1):data[0]); 
            setIsManager((data[0].indexOf("*")!=-1)?true:false); 
            setDateHired(dfStorage.parse(data[1]));
            setDepartment(data[2]);            
            setWorkdays(getWeekdayArray(data[3]));
        } catch (ParseException ex) {
            throw new InvalidValueException("Parsing error");
        }
    }
    
    Weekday[] getWeekdayArray(String str){
        String [] strArr = str.split(",");
        Weekday[] wdArr = new Weekday[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            wdArr[i] = Weekday.valueOf(strArr[i]);
        }
        return wdArr;
    } 

    void setWorkdays(Weekday[] workdaysArray){
        for (int i = 0; i < workdaysArray.length; i++) {
            workdaysList.add(workdaysArray[i]);
        }        
    }    
    
    final static SimpleDateFormat dfDisplay = new SimpleDateFormat("MMM dd yyyy");
    final static SimpleDateFormat dfStorage = new SimpleDateFormat("yyyy-MM-dd");
    
    public void setName(String name) throws InvalidValueException {
        if(!name.matches(".[^;^?@!~*]{2,50}$")){
            throw new InvalidValueException("Task must be between 1-100 only some characters.");
        }        
        this.name = name;
    }

    public void setIsManager(boolean isManager) {
        this.isManager = isManager;
    }

    public void setDepartment(String department) throws InvalidValueException {
        if(!name.matches(".[^;^?@!~*]{2,50}$")){
            throw new InvalidValueException("Task must be between 1-100 only some characters.");
        }   
        this.department = department;
    }

    public void setDateHired(Date dateHired) throws InvalidValueException {
        try {
            Date min = new SimpleDateFormat("yyyy-MM-dd").parse("1900-01-01");
            Date max = new SimpleDateFormat("yyyy-MM-dd").parse("2100-12-31");

            if(min.compareTo(dateHired) * dateHired.compareTo(max) < 0){
                throw new InvalidValueException("Date Of Birth must be between 1900-2100.");
            }
        } catch (ParseException e) {
            throw new InvalidValueException("Date Of Birth must be between 1900-2100.");
        }
        this.dateHired = dateHired;
    }

    public String getName() {
        return name;
    }

    public boolean isIsManager() {
        return isManager;
    }

    public String getDepartment() {
        return department;
    }

    public Date getDateHired() {
        return dateHired;
    }

    String getStrArrWorkdays(ArrayList<Weekday> workdaysList){
        ArrayList<String> strArrList = new ArrayList();
        for (int i = 0; i < workdaysList.size(); i++) {
            strArrList.add(workdaysList.get(i).toString());
        }
        return String.join(",", strArrList);
    }
    
    String toDataString() {
        return String.format("%s%s;%s;%s;%s", name, isManager?"*":"", dfStorage.format(dateHired),department,getStrArrWorkdays(workdaysList));
    }
    
    @Override
    public String toString() {
        return String.format("%s, %s of %s, hired %s work on %s",
                name, isManager?"manager":"employee",department,dfDisplay.format(dateHired),getStrArrWorkdays(workdaysList));
    }


}
