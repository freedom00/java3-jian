package employeesdb;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Database {

    private Connection dbConn;

    Database() throws SQLException {
            dbConn = DriverManager.getConnection("jdbc:mysql://localhost/ipd20todos?user=root&password=root");

    } // constructor will connect to database, initialize dbConn

    ArrayList<Todo> getAllTodos() throws SQLException {
        ArrayList<Todo> td = new ArrayList<>();
        try{
            PreparedStatement select = dbConn.prepareStatement("select * from todos");
            ResultSet resultSet = select.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String task = resultSet.getString("task");
                int difficulty = resultSet.getInt("difficulty");
                String status = resultSet.getString("status");
                java.sql.Date dueDate = resultSet.getDate("dueDate");
                Todo t = new Todo(id,task,difficulty,dueDate,Status.valueOf(status));
                //int id,String task, int difficulty, Date dueDate, Status status
                td.add(t);
            }
            resultSet.close();
            select.close();
        }catch (SQLException|InvalidValueException ex) {
            throw new SQLException(ex.getMessage());
        }
        return td;
    }
    
    void addTodo(Todo todo) throws SQLException {
        try{
            PreparedStatement insert = dbConn.prepareStatement("insert into todos values(NULL,?,?,?,?)");
            insert.setString(1, todo.task);
            insert.setInt(2, todo.difficulty);
            insert.setDate(3, new java.sql.Date(todo.dueDate.getTime()));
            insert.setString(4, todo.status.toString());
            insert.execute();
            insert.close();
        }catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }
    
    void updateTodo(Todo todo) throws SQLException {
        try{
            PreparedStatement update = dbConn.prepareStatement("update todos set task=?,difficulty=?,dueDate=?,status=? where id=?");
            update.setString(1, todo.task);
            update.setInt(2, todo.difficulty);
            update.setDate(3, new java.sql.Date(todo.dueDate.getTime()));
            update.setString(4, todo.status.toString());
            update.setInt(5, todo.id);
            update.execute();
            update.close();
        }catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }
    void deleteTodo(int id) throws SQLException {
        try{
            PreparedStatement delete = dbConn.prepareStatement("delete from todos where id=?");
            delete.setInt(1, id);
            delete.execute();
            delete.close();
        }catch (SQLException ex) {
            throw new SQLException(ex.getMessage());
        }
    }
    
}
