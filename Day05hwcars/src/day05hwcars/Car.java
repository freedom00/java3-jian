package day05hwcars;

import java.text.ParseException;
import java.util.Comparator;

/**
 *
 * @author trakadmin
 */
public class Car implements Comparable<Car> {
    enum FuelType{Gasoline, Dieasel, Hybrid, Electric, Other}
    
    int id;
    String makeModel;
    double engineSize;
    FuelType fuelType;

    public Car(int id,String model, double engineSize, FuelType fuelType) throws InvalidValueException {
        setId(id);
        setMakeModel(model);
        setEngineSize(engineSize);
        setFuelType(fuelType);
    }
    
    public Car(String dataLine) throws InvalidValueException{
        String[] data = dataLine.split(";");
        if (data.length != 4) {
            throw new InvalidValueException("Invalid number of items in data line"+dataLine);
        }
        try{
            setId(Integer.parseInt(data[0])); // ex
            setMakeModel(data[1]); // ex NumberFormatException
            setEngineSize(Double.parseDouble(data[2])); // ex ParseException
            setFuelType(FuelType.valueOf(data[3]));
        }catch(IllegalArgumentException ex){
            throw new InvalidValueException("Invalid number of items in data line"+dataLine);
        }
        
    }
    
    public static int count=1;

    public void setId(int id) {
        this.id = id;
    }

    public void setMakeModel(String makeModel) {
        this.makeModel = makeModel;
    }

    public void setEngineSize(double engineSize) {
        this.engineSize = engineSize;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }


    public int getId() {
        return id;
    }

    public String getMakeModel() {
        return makeModel;
    }

    public double getEngineSize() {
        return engineSize;
    }

    public FuelType getFuelType() {
        return fuelType;
    }
    
    String toDataString() {
        return String.format("%d;%s;%.1f;%s", id, makeModel, engineSize,fuelType);
    }
    
    @Override
    public String toString() {
        return String.format("%d: %s %.1f %s",
                id, makeModel,engineSize,fuelType);
    }   
    
    @Override
    public int compareTo(Car other) { // by name
        return Comparator.comparingInt(Car::getId)
                  .thenComparing(Car::getMakeModel)
                  .thenComparingDouble(Car::getEngineSize)
                  .compare(this, other);
        //return makeModel.compareTo(other.makeModel);
    }
    
    final static Comparator<Car> comparatorByEngineSize = new Comparator<Car>() {
        @Override
        public int compare(Car p1, Car p2) {
            if(p1.engineSize>p2.engineSize)
                return 1;
            else if(p1.engineSize<p2.engineSize)
                return -1;
            else
                return 0;
        }
    };

    final static Comparator<Car> comparatorByFueltype = new Comparator<Car>() {
        @Override
        public int compare(Car p1, Car p2) {
            return p1.getFuelType().toString().compareTo(p2.getFuelType().toString());
        }
    };
}

class InvalidValueException extends Exception {
	InvalidValueException(String msg) { super(msg); }
}
